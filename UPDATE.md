Laravel MongoDB
===============

Query\Builder

```php
    /**
     * Convert a key to ObjectID if needed.
     *
     * @param  mixed $id
     * @return mixed
     */
    public function convertKey($id)
    {
        // Use ObjectId in string mode
        // if (is_string($id) && strlen($id) === 24 && ctype_xdigit($id)) {
        //     return new ObjectID($id);
        // }

        return $id;
    }
```

Relations\EmbedsOneOrMany

```php
    /**
     * @inheritdoc
     */
    protected function getEmbedded()
    {
        // Get raw attributes to skip relations and accessors.
        $attributes = $this->parent->getAttributes();

        // Get embedded models form parent attributes.
        $embedded = isset($attributes[$this->localKey]) ? (array) $attributes[$this->localKey] : $this->related;

        return $embedded;
    }
```